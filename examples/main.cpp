#include <iostream>
#include <string>

#include <vtkio>
#include "Eigen/Eigen/Dense"


int main()
{
	// Example one: Triangular mesh
	{
		// Create a VTKFile
		vtkio::VTKFile vtk_file;

		// Load a file
		vtk_file.read("../res/cloth.vtk");

		// Extract information into "indexable" containers
		std::vector<Eigen::Vector3d> vertices; // Make sure the points type match (float/double)
		std::vector<std::array<int, 3>> triangles;
		std::vector<Eigen::Vector3d> normals;
		vtk_file.get_points_to_twice_indexable(vertices);
		const vtkio::CellType cell_type = vtk_file.get_cells_to_twice_indexable(triangles);
		const vtkio::AttributeType attr_type = vtk_file.get_point_data_to_twice_indexable("normals", normals);
		
		// Modify the data and create new one
		//// Move points upwards in Z direction
		for (auto& v : vertices)
			v += Eigen::Vector3d::UnitZ();

		//// Create a 1D scalar field using the Z component
		std::vector<double> z;
		for (auto& v : vertices)
			z.push_back(v[2]);

		//// Get normals from the point data and turn the component Z around
		for (auto& n : normals)
			n[2] *= -1.0;

		// Set the modified and new data into the file
		vtk_file.set_points_from_twice_indexable(vertices);
		vtk_file.set_cells_from_twice_indexable(triangles, vtkio::CellType::Triangle);  // This is unnecessary since the triangle data is already in the buffer
		vtk_file.set_point_data_from_indexable("z", z, vtkio::AttributeType::Scalars);
		vtk_file.set_point_data_from_twice_indexable("normals", normals, vtkio::AttributeType::Normals);  // Note: Paraview use the AttributeType::Normals for shading

		// Write
		vtk_file.write("../res/cloth_out.vtk");
	}


	// Example 2: SPH particle data
	{
		// Create a VTKFile
		vtkio::VTKFile vtk_file;

		// Load a file
		vtk_file.read("../res/sph_particles.vtk");

		// Extract information into "indexable" containers
		std::vector<Eigen::Vector3f> particles;
		std::vector<uint32_t> particle_id;
		std::vector<float> density;
		std::vector<std::array<float, 3>> velocity;
		vtk_file.get_points_to_twice_indexable(particles);
		vtk_file.get_point_data_to_indexable("id", particle_id);
		vtk_file.get_point_data_to_indexable("density", density);
		vtk_file.get_point_data_to_twice_indexable("velocity", velocity);

		// Clear the file and fill a new one from scratch
		vtk_file.clear();
		vtk_file.set_points_from_twice_indexable(particles);
		vtk_file.set_cells_as_particles(particles.size()); // Convenient way to specify that we don't have cells, just points
		vtk_file.set_comments("Particle radius: 0.01");

		// Write
		vtk_file.write("../res/sph_particles_out.vtk");
	}

	// Example 3: Direct usage of std::vector instead of (Twice)Indexable templated container
	{
		// Create a VTKFile
		vtkio::VTKFile vtk_file;

		// Load a file
		vtk_file.read("../res/cloth.vtk");

		// Extract information into std::vector flat containers
		std::vector<double> vertices;
		vtk_file.get_points_raw(vertices);

		std::vector<int> cell_array;  // Not exactly triangles. (See .vtk specification for details on cell array representation)
		vtk_file.get_cells_raw(cell_array);

		std::vector<int> cell_types_array;  // See .vtk specification for details on cell types array representation
		vtk_file.get_cell_types_raw(cell_types_array);

		std::vector<double> normals;
		vtk_file.get_point_data_raw("normals", normals);

		// Clear
		vtk_file.clear();

		// Set everything back and write
		vtk_file.set_points_raw(vertices);
		vtk_file.set_cells_raw(cell_array);
		vtk_file.set_cell_types_raw(cell_types_array);
		vtk_file.set_point_data_raw("normals", normals, vtkio::AttributeType::Normals);

		// Write
		vtk_file.write("../res/cloth_raw_out.vtk");
	}

	// Example 4: Regular grid to sample a scalar field. (Check marching cubes result in Paraview)
	{
		// Create a VTKFile containing a regular grid
		vtkio::VTKFile vtk_file = vtkio::VTKFile::regular_grid<float>({ 0, 0, 0 }, { 1, 1, 1 }, {20, 20, 20});

		// Get vertices
		std::vector<std::array<float, 3>> vertices;
		vtk_file.get_points_to_twice_indexable(vertices);

		// Evaluate a level set function
		const int n_points = (int)vertices.size();
		std::vector<float> level_set(n_points);
		for (int i = 0; i < n_points; i++) {
			level_set[i] = std::sin(5.0f*vertices[i][0]) + std::cos(5.0f*vertices[i][1]) + std::sin(5.0f*vertices[i][2])*std::cos(5.0f*vertices[i][2]);
		}
		vtk_file.set_point_data_from_indexable("level_set", level_set, vtkio::AttributeType::Scalars);

		// Write
		vtk_file.write("../res/regular_grid.vtk");
	}

	return 0;
}