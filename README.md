# vtkio
`vtkio` is a header-only C++ library to read and write .vtk files.

## Hello world
### Read
```cpp
// Create a VTKFile
vtkio::VTKFile vtk_file;

// Load a file
vtk_file.read("path/to/file.vtk");

// Extract information
std::vector<std::array<double, 3>> vertices;
vtk_file.get_points_to_twice_indexable(vertices);

std::vector<std::array<int, 3>> triangles;
const vtkio::CellType cell_type = vtk_file.get_cells_to_twice_indexable(triangles);

std::vector<double> point_data;
const vtkio::AttributeType attr_type = vtk_file.get_point_data_to_indexable("data", data);
```

### Write
```cpp
// Create a VTKFile
vtkio::VTKFile vtk_file;

// Set information
vtk_file.set_points_from_twice_indexable(vertices);
vtk_file.set_cells_from_twice_indexable(triangles, vtkio::CellType::Triangle);
vtk_file.set_point_data_from_indexable("data", data, vtkio::AttributeType::Scalars);

// Write
vtk_file.write("path/to/file.vtk");
```

## What you need to know about vtkio
### `ByteBuffer` and `VTKFile`
`vtkio` is composed of two main classes: `ByteBuffer` and `VTKFile`. `ByteBuffer` is a utility class used to read, write and store the data and deal with its binary representation, while `VTKFile` is merely an interface that facilitates the interaction with the different buffers (points, cells and data). For most cases, the interface provided in `VTKFile` will be enough.

### `Indexable` and `TwiceIndexable`
`vtkio` extensively uses the templated types `Indexable` and `TwiceIndexable` as interface to the most common data structure you will likely use to store points, cells and data in your programs. The reason is so you can use those types directly in `vtkio` without the need of a intermediate container, avoiding unnecessary code and data duplicities. Both `Indexable` and `TwiceIndexable` refer to dynamically sized arrays, the main difference being that `Indexable` contains numbers directly while `TwiceIndexable` contains fix sized arrays. Furthermore, they should both implement `.size()` and `.resize()`.

Examples of `Twiceindexable` are:
- `std::vector<Eigen::Vector3d>`
- `std::vector<std::array<float, 3>>`
- `Eigen::Matrix<float, -1, 3>`

Examples of `Indexable` are:
- `std::vector<double>`
- `Eigen::VectorXf`

Naturally, `TwiceIndexable` can be used only when the data to be set or get has a fixed number of columns, which cover most of the use cases. For exceptions, like a mesh with mixed cell types of different number of nodes (tetrahedra and hexahedra), the "raw" interface of `VTKFile` should be used instead to set and get data directly from and to `std::vector<T>`. However, this will require that you know the nuances of the vtk data format (specially about the cell array). Also keep in mind that if you use incompatible containers you will receive template compilation errors that might not be easy to trace. 

### Make sure to match the data type when "getting"
It is possible to read from file any number type vtk supports. However, when getting data from a `VTKFile` into a container, the type must match exactly. You will get errors when trying to get data that was written as `double` in the .vtk file into a `float` container. Same applies to `int` and `uint`.

### Only set what changes
If you want to write a sequence of files, you don't have to update the buffers that do not change, which can save a significant amount of time. For example, to write a sequence of files containing a cloth simulation where the triangle connectivity data is constant, one can just `VTKFile.set_cells_...()` once at the beginning and only update the vertex data.

## Examples
You can find more examples in the "examples" folder.

## Limitations
- Only DATASET UNSTRUCTURED_GRID is supported.